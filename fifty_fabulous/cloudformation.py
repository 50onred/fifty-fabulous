
from datetime import datetime, timedelta


# Test Stack Expiration utilities

EXPIRATION_TAG_NAME = 'TestExpiration'


def expiration_tag(**timedelta_args):
    """
    See datetime.timedelta from the standard library for argument details.

    Defaults to 1 hour.
    """
    return {
        'Key': EXPIRATION_TAG_NAME,
        'Value': expiration_value(**timedelta_args)
    }


def expiration_value(**timedelta_args):
    """
    See datetime.timedelta from the standard library for argument details.

    Defaults to 1 hour.
    """
    if not timedelta_args:
        kwargs = {'hours': 1}
    else:
        kwargs = {key: float(value) for key, value in timedelta_args.items()}

    expiration = (datetime.utcnow() + timedelta(**kwargs)).replace(microsecond=0)

    return expiration.isoformat()
