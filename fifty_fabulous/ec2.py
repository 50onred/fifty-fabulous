import boto3


def get_instances_by_tag(tag, value, boto3_session=None):
    boto = boto3_session or boto3
    client = boto.client('ec2')

    custom_filter = [{
        'Name': 'tag:{}'.format(tag),
        'Values': [value]
    }]

    response = client.describe_instances(Filters=custom_filter)

    instances = []
    for r in response['Reservations']:
        for i in r['Instances']:
            if i['State']['Name'] == "running":
                instances.append(i)
    return instances
