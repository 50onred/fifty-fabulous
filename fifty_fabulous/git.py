from fabric.operations import local


def get_commit_hash():
    """ Get the locally checked out git branch. Suffix with `-dirty` when relevant. """
    current_hash = local('git log -1 --pretty=%H', capture=True)
    dirty = '-dirty' if local('git status --porcelain', capture=True) else ''
    return '{}{}'.format(current_hash, dirty)

def get_git_branch_name():
    branch_name = local('git symbolic-ref --short -q HEAD', capture=True)
    if branch_name == 'master':
        return get_commit_hash()
    return branch_name

