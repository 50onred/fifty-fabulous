import json
import re

from fabric.operations import prompt
import requests

from .git import get_commit_hash

PRODUCT_CHOICES = [u'dash', u'pops', u'native', u'push', u'search', u'etl', u'go-wabbit', 'injections']


def _valid_title(raw_title):
    if not len(raw_title.strip()):
        raise Exception(u'Title is required')
    if len(raw_title) > 100:
        raise Exception(u'Title cannot exceed 100 characters')
    return raw_title.strip()


def _valid_product(raw_product):
    tokens = set(re.split(r'[^a-zA-Z-]', raw_product))
    matching = tokens.intersection(PRODUCT_CHOICES)
    if not matching:
        raise Exception(u'No valid products found')
    return ", ".join(sorted(matching))


def changelog_notify(webhook_url, token, default_product, default_repo, commit_hash=None):
    """ Post to changelog for deploy """

    commit_hash = commit_hash or get_commit_hash()

    products = prompt(u"Products, one or more of {} (required): ".format(PRODUCT_CHOICES), default=default_product, validate=_valid_product)
    repo = prompt(u"Repository (required): ", default=default_repo)
    title = prompt(u"Title (required): ", validate=_valid_title)
    issue = prompt(u"Issue ID: ")
    description = prompt(u"Description: ")

    resp = requests.post(
        webhook_url,
        headers={
            u'Authorization': u'Bearer {token}'.format(token=token),
            u'Content-Type': u'application/json'
        },
        data=json.dumps({
            u"products": products,
            u"repository": repo,
            u"title": title,
            u"issue": issue,
            u"description": description,
            u"commit": commit_hash,
        })
    )
    if resp.status_code != 200:
        print(u"Changelog update failed")
        print(resp.content)
