from collections import defaultdict

import boto3
import fabric
from fabric.decorators import serial, runs_once
from fabric.operations import env, run, local

from .git import get_commit_hash


def get_git_container_tag():
    """ Static prefix to allow for git-based tag ECR rules. Currently rules language is simple and can only
    prefix-match. """
    return 'git-{}'.format(get_commit_hash())


def get_ecs_info(cluster_name, service_name, boto3_session=None):
    """
    Given a cluster and service name, load env.tasks with each running task from that service on the cluster and
    env.hosts with the corresponding hosts.

    The env.tasks and env.hosts arrays are synchronized so a task index can be used to find the corresponding host.
    """

    # This is required as multiple tasks can be on a given host.
    env.dedupe_hosts = False

    boto = boto3_session or boto3
    ecs = boto.client('ecs')
    ec2 = boto.client('ec2')

    try:
        task_arns = ecs.list_tasks(cluster=cluster_name, serviceName=service_name)['taskArns']
    except ecs.exceptions.ServiceNotFoundException:
        print('Service not found: {}'.format(service_name))
        return
    task_details_res = ecs.describe_tasks(cluster=cluster_name, tasks=task_arns)

    # Main List
    env.tasks = [task for task in task_details_res['tasks'] if task['lastStatus'] == 'RUNNING']

    instance_arns = [task['containerInstanceArn'] for task in env.tasks]
    container_inst_res = ecs.describe_container_instances(cluster=cluster_name,
                                                          containerInstances=instance_arns)
    ec2_ids = [instance['ec2InstanceId']
               for instance in container_inst_res['containerInstances']
               if instance['status'] == 'ACTIVE']

    instance_id_to_arn = {}
    for instance in container_inst_res['containerInstances']:
        instance_id_to_arn[instance['ec2InstanceId']] = instance['containerInstanceArn']

    hosts_res = ec2.describe_instances(InstanceIds=ec2_ids)

    # Mapping for Ip info
    env.instance_arn_to_ip = {}
    for reservation in hosts_res['Reservations']:
        for instance in reservation['Instances']:
            instance_id = instance['InstanceId']
            if instance_id in instance_id_to_arn:
                env.instance_arn_to_ip[instance_id_to_arn[instance_id]] = instance['PublicIpAddress']
    _set_hosts_from_tasks()


def image_exists(ecr_registry_id, repo_name, tag, boto3_session=None):
    """ Returns image manifest for retagging with deployment tag. """
    boto = boto3_session or boto3
    ecr = boto.client('ecr')
    images = ecr.batch_get_image(
        registryId=ecr_registry_id,
        repositoryName=repo_name,
        imageIds=[{'imageTag': tag}]
    )

    if not images['images']:
        return

    manifest = images['images'][0]['imageManifest']
    return manifest


def list_deploy_tags(ecr_registry_id, ecr_repo_name, prefix_tuples, boto3_session=None):
    """
    Prints out latest deploy tags for each service with the associated git tags.

    Useful for determining what code was deployed when, and figuring out what to deploy for rolling back a change.


    Example prefix_mapping:
    [('deploy-rtb-daemon-tracker-', 'tracker'), ('deploy-rtb-daemon-bidder-', 'external-bidder')]

    This example would group deploy tags with the headers 'tracker' and 'external-bidder'. It would use the
    corresponding prefix to identify which deploy tags go to which service. Prefixes are checked in order, first wins.

    :param ecr_registry_id:
    :param ecr_repo_name:
    :param prefix_tuples: a list of tuples of "tag prefix" to associated service
    :return:
    """
    boto = boto3_session or boto3
    ecr = boto.client('ecr')
    iterator = ecr.get_paginator('describe_images').paginate(
        registryId=ecr_registry_id,
        repositoryName=ecr_repo_name,
        filter={
            'tagStatus': 'TAGGED'
        }
    )

    deployments_by_service = defaultdict(list)
    for item in iterator:
        for details in item['imageDetails']:
            image_git_tag = ''
            # Sort in reverse to put `git-` tag before `deploy-` tags
            for tag in sorted(details['imageTags'], reverse=True):
                if tag.startswith('git-'):
                    image_git_tag = tag
                    continue

                for prefix, service_name in prefix_tuples:
                    if tag.startswith(prefix):
                        deployments_by_service[service_name].append((tag, image_git_tag))
                        break

    for service, deployments in deployments_by_service.iteritems():
        print('Service: {}'.format(service))
        for tag, git_tag in sorted(deployments, reverse=True):
            print('    - {}  ({})'.format(tag, git_tag))
        print()


## Service selector filters
@runs_once
def first():
    env.tasks = [env.tasks[0]]
    _set_hosts_from_tasks()


@runs_once
def ith(num):
    env.tasks = [env.tasks[int(num)]]
    _set_hosts_from_tasks()


@runs_once
def new():
    """
    During a deploy, selects tasks with a higher task definition version. Used after a service selector (bidder)
    """
    versions = _task_definition_versions()
    if len(versions) != 2:
        fabric.utils.error("`new` expects '2' task definition versions to be active, but found '{}'.".format(len(versions)))
    new_version = ':{}'.format(versions[-1])
    env.tasks = [task for task in env.tasks if task['taskDefinitionArn'].endswith(new_version)]
    _set_hosts_from_tasks()


@runs_once
def old():
    """
    During a deploy, selects tasks with a lower task definition version. Used after a service selector (bidder)
    """
    versions = _task_definition_versions()
    if len(versions) != 2:
        fabric.utils.error("`old` expects '2' task definition versions to be active, but found '{}'.".format(len(versions)))
    old_version = ':{}'.format(versions[0])
    env.tasks = [task for task in env.tasks if task['taskDefinitionArn'].endswith(old_version)]
    _set_hosts_from_tasks()


## Debugging commands
def logs():
    """ Tail a tasks logs """
    run('docker logs -f `{}` --since "5s"'.format(_current_container_id_cmd()))


def container():
    """ ssh into task container"""
    run('docker exec -it `{}` bash'.format(_current_container_id_cmd()))


@runs_once
def info():
    """
    Prints out task connectivity information (host ip + port).

    Useful for sending test requests directly to a single task or for JMX connections.
    """
    for task in env.tasks:
        print('----------------------')
        _print_task_info(task)

@serial
def ssh():
    """ open shell to remote ec2 """
    local("ssh %s@%s" % (env.user, env.host))


def _set_hosts_from_tasks():
    env.hosts = [env.instance_arn_to_ip.get(task['containerInstanceArn']) for task in env.tasks]
    _print_tasks_hosts()


def _print_tasks_hosts():
    print([task['taskArn'] for task in env.tasks])
    print(env.hosts)
    print()


def _print_task_info(task):
    print(task['taskArn'])
    print(task['taskDefinitionArn'])
    ip = env.instance_arn_to_ip.get(task['containerInstanceArn'])
    if not ip:
        print("Missing IP address.")
        ip = 'missing'

    print()
    for container in task['containers']:
        print('Container: {}'.format(container['name']))
        for binding in container['networkBindings']:
            print('Port {container_port} bound to {ip}:{host_port} ({protocol})'.format(
                protocol=binding['protocol'],
                container_port=binding['containerPort'],
                ip=ip,
                host_port=binding['hostPort']
            ))
        print()


def _current_task_arn():
    """ Fab provides the env.host automatically. Because we use the same index we can look up the task with that."""
    return env.tasks[env.hosts.index(env.host)]['taskArn']


def _current_container_id_cmd():
    """ Must be used in the context of a fab command (run or sudo)"""

    # TODO: Handle multi-container tasks
    return 'docker ps --filter "label=com.amazonaws.ecs.task-arn={}" -q'.format(_current_task_arn())


def _task_definition_versions():
    versions = sorted(set(int(task['taskDefinitionArn'].split(':')[-1]) for task in env.tasks))
    print("Task Definition Versions: {}".format(versions))
    return versions
