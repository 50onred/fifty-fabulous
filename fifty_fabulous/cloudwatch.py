from datetime import datetime
import time

import boto3


def tail(group_name, time_fmt="%Y-%m-%d %H:%M:%S", boto3_session=None):
    boto = boto3_session or boto3
    client = boto.client('logs')
    stream_batch = client.describe_log_streams(logGroupName=group_name, orderBy='LastEventTime', descending=True, limit=1)
    stream = stream_batch['logStreams'][0]
    log_stream_name = stream['logStreamName']
    next_token = None
    sleep_time = 0

    while True:
        kwargs = dict(logGroupName=group_name, logStreamName=log_stream_name)
        if next_token:
            kwargs['nextToken'] = next_token
        log_events = client.get_log_events(**kwargs)

        for event in log_events['events']:
            dt = datetime.fromtimestamp(event['timestamp'] / 1000)
            print("{}\t{}".format(dt.strftime(time_fmt), event['message']))

        # set token for next page
        next_token = log_events['nextForwardToken']

        # once we've exhausted events, start sleeping
        if len(log_events['events']) == 0:
            sleep_time = 5

        time.sleep(sleep_time)
