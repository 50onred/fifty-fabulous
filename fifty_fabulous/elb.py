import sys
import time
import boto3


STATE_IN_SERVICE = 'InService'
STATE_OUT_OF_SERVICE = 'OutOfService'
STATE_UNKNOWN = 'Unknown'


def register_to_elb(elb_name, instance_id, awaited_state=STATE_IN_SERVICE, boto3_session=None):
    boto = boto3_session or boto3
    client = boto.client('elb')
    try:
        client.register_instances_with_load_balancer(
            LoadBalancerName=elb_name,
            Instances=[
                {
                    'InstanceId': instance_id
                },
            ]
        )
    except Exception as e:
        print(e)
        print("ELB register failed for {}".format(elb_name))
        return

    if awaited_state:
        await_elb_state(elb_name, instance_id, awaited_state)


def deregister_from_elb(elb_name, instance_id, awaited_state=STATE_OUT_OF_SERVICE, boto3_session=None):
    boto = boto3_session or boto3
    client = boto.client('elb')
    try:
        client.deregister_instances_from_load_balancer(
            LoadBalancerName=elb_name,
            Instances=[
                {
                    'InstanceId': instance_id
                },
            ]
        )
    except Exception as e:
        print(e)
        print("ELB deregister failed for {}".format(elb_name))
        return

    if awaited_state:
        await_elb_state(elb_name, instance_id, awaited_state)


def await_elb_state(elb_name, instance_id, awaited_state, boto3_session=None):
    boto = boto3_session or boto3
    client = boto.client('elb')
    while True:
        response = client.describe_instance_health(
            LoadBalancerName=elb_name,
            Instances=[
                {
                    'InstanceId': instance_id
                },
            ]
        )

        state = response['InstanceStates'][0]['State']
        if state == awaited_state:
            break
        else:
            if sys.version_info.major >= 3:
                print(".", end="", flush=True)
            else:
                print("."),
                sys.stdout.flush()
            time.sleep(1)
