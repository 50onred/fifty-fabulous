from setuptools import find_packages, setup

setup(
    name='Fifty-Fabulous',
    version='0.0.17',
    url='https://bitbucket.org/50onred/fifty-fabulous',
    license='BSD',
    author='Rob Harrigan',
    author_email='rharrigan@50onred.com',
    description='Shared library for fabric deploys',
    long_description=__doc__,
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'requests',
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
